from typing import NamedTuple

TYPE_PREVIEW = 'preview'
TYPE_DEFAULT = 'default'
TYPE_BACK = 'back'
TYPE_LEFT = 'left'
TYPE_RIGHT = 'right'
TYPE_LABEL = 'label'
TYPE_LABEL_INSIDE = 'label_inside'
TYPE_LABEL_OUTSIDE = 'label_outside'


class FileItem(NamedTuple):
    id: str
    type: str
    hash: str
    url: str
    filename: str
    mime_type: str
    size: int
    width: int
    height: int
    dpi: int
    """
    ok - file was processed successfully
    waiting - file is being processed
    failed - file failed to be processed
    """
    status: str
    # Timestamp
    created: int
    thumbnail_url: str
    preview_url: str
    # Show file in the Printfile Library (default true)
    visible: bool

    @staticmethod
    def from_json(raw: dict):
        return FileItem(
            id=raw.get('id'),
            type=raw.get('type'),
            hash=raw.get('hash'),
            url=raw.get('url'),
            filename=raw.get('filename'),
            mime_type=raw.get('mime_type'),
            size=raw.get('size'),
            width=raw.get('width'),
            height=raw.get('height'),
            dpi=raw.get('dpi'),
            status=raw.get('status'),
            created=raw.get('created'),
            thumbnail_url=raw.get('thumbnail_url'),
            preview_url=raw.get('preview_url'),
            visible=raw.get('visible')
        )


    def to_json(self) -> dict:
        return {
            'id': self.id,
            'type': self.type,
            'url': self.url,
            'filename': self.filename,
            'visible': self.visible
        }
