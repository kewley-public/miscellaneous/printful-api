from typing import List, Optional

from printful_api import PrintfulApiClient
from printful_api.model.webhook.webhook_item import WebhookItem
from printful_api.model.webhook.webhooks_info_item import WebhooksInfoItem


class WebhookApi:
    """
    API Docs: https://www.printful.com/docs/webhooks
    Simulator: https://www.printful.com/docs/webhook-simulator
    """

    def __init__(self, client: PrintfulApiClient):
        self.client = client

    @staticmethod
    def parse_webhook(raw: dict) -> WebhookItem:
        f"""
        Will parse a request to a webhook

        :param raw: The request sent from printful to your webhook
        :return: {WebhookItem}
        """
        return WebhookItem.from_json(raw)

    def get_registered_webhooks(self):
        f"""
        Performs a GET on Webhooks to retrieve the current registered hook
        
        :return: {WebhooksInfoItem}
        """
        response = self.client.get('webhooks')
        return WebhooksInfoItem.from_json(response)

    def register_webhooks(self, url: str, types: List[str], params: Optional[List[any]] = None) -> WebhooksInfoItem:
        f"""
        Allows to enable webhook URL for the store and select webhook event types that will be sent to this URL.
        Only one webhook URL can be active for a store, so calling this method disables all existing webhook configuration.

        :param url: Webhook URL that will receive store's event notifications
        :param types: Array of enabled webhook event types
        :param params: Optional array of parameters for enabled webhook event types
        :return: {WebhooksInfoItem}
        """
        request_data = {
            'url': url,
            'types': types
        }
        if params:
            request_data['params'] = params
        response = self.client.post('webhooks', request_data)
        return WebhooksInfoItem.from_json(response)

    def disable_webhooks(self):
        f"""
        Will disable all registered webhooks
        
        :return: {WebhooksInfoItem}
        """
        response = self.client.delete('webhooks')
        return WebhooksInfoItem.from_json(response)
