from typing import Optional, NamedTuple


class RecipientCreationParameters(NamedTuple):
    name: str
    company: str
    address1: str
    address2: Optional[str]
    city: str
    state_code: str
    state_name: str
    country_code: str
    country_name: str
    zip: str
    phone: str
    email: str

    def to_json(self) -> dict:
        return {
            'name': self.name,
            'company': self.company,
            'address1': self.address1,
            'address2': self.address2,
            'city': self.city,
            'state_code': self.state_code,
            'state_name': self.state_name,
            'country_code': self.country_code,
            'country_name': self.country_name,
            'zip': self.zip,
            'phone': self.phone,
            'email': self.email
        }
