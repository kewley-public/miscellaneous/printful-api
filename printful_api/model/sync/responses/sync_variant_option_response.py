from typing import NamedTuple


class SyncVariantOptionResponse(NamedTuple):
    id: str
    value: any

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantOptionResponse(
            id=raw.get('id'),
            value=raw.get('value')
        )
