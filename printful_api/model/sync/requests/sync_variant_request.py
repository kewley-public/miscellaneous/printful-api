from typing import NamedTuple, Optional, List

from printful_api.exception.printful_sdk_exception import PrintfulSdkException
from printful_api.model.sync.requests.sync_variant_request_file import SyncVariantRequestFile
from printful_api.model.sync.requests.sync_variant_request_option import SyncVariantRequestOption


class SyncVariantRequest(NamedTuple):
    id: Optional[int]
    external_id: Optional[str]
    variant_id: Optional[int]
    retail_price: Optional[float]
    files: List[SyncVariantRequestFile]
    options: List[SyncVariantRequestOption]

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantRequest(
            id=raw.get('id'),
            external_id=raw.get('external_id'),
            variant_id=raw.get('variant_id'),
            retail_price=raw.get('retail_price'),
            files=[SyncVariantRequestFile.from_json(x) for x in raw.get('files', [])],
            options=[SyncVariantRequestOption.from_json(x) for x in raw.get('options', [])]
        )

    def add_file(self, file: SyncVariantRequestFile):
        if self.files:
            self.files.append(file)
        else:
            self.files = [file]

    def add_option(self, option: SyncVariantRequestOption):
        if self.options:
            self.options.append(option)
        else:
            self.options = [option]

    def to_post_json(self) -> dict:
        files = [x.to_json() for x in self.files]
        options = [x.to_json() for x in self.options]

        if self.variant_id is None:
            raise PrintfulSdkException('Missing variant_id')

        if len(files):
            raise PrintfulSdkException('Missing files')

        return {
            'external_id': self.external_id,
            'retail_price': self.retail_price,
            'variant_id': self.variant_id,
            'files': files,
            'options': options
        }

    def to_put_json(self) -> dict:
        put_json = {}
        if self.external_id:
            put_json['external_id'] = self.external_id

        if self.retail_price:
            put_json['retail_price'] = self.retail_price

        if self.variant_id is None:
            raise PrintfulSdkException('Variant id is required')

        if self.files and len(self.files):
            put_json['files'] = [x.to_json() for x in self.files]

        if self.options and len(self.options):
            put_json['options'] = [x.to_json() for x in self.options]

        put_json.update({
            'variant_id': self.variant_id
        })
        return put_json
