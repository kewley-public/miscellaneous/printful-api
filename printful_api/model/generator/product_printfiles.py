from typing import NamedTuple, List

from printful_api.model.generator.printfile_item import PrintfileItem
from printful_api.model.generator.variant_printfile_item import VariantPrintfileItem


class ProductPrintfiles(NamedTuple):
    product_id: int
    """
    List of available file placements (front, back, etc...) for this file.
    Key is placement id, value is display title
    """
    available_placements: dict
    """
    List of all unique printfiles for this product
    """
    printfiles: List[PrintfileItem]
    variant_printfiles = List[VariantPrintfileItem]

    @staticmethod
    def from_json(raw: dict):
        printfiles = []
        if raw.get('printfiles'):
            printfiles = [PrintfileItem.from_json(x) for x in raw.get('printfiles', [])]

        variant_printfiles = []
        if raw.get('variant_printfiles'):
            variant_printfiles = [VariantPrintfileItem.from_json(x) for x in raw.get('variant_printfiles', [])]

        return ProductPrintfiles(
            product_id=raw.get('product_id'),
            available_placements=raw.get('available_placements'),
            printfiles=printfiles,
            variant_printfiles=variant_printfiles
        )
