from typing import NamedTuple


class VariantTemplateMappingItem(NamedTuple):
    """ Printful product variant id """
    variant_id: int
    """ 
    Placements mapped by template ids (e.g. {placement => template_id, ....})
    Use this dict to find out which templates have to be used for this product variant.
    """
    templates: dict

    @staticmethod
    def from_json(raw: dict):
        templates = {}
        for template in raw.get('templates', []):
            templates[template['placement']] = int(template['template_id'])

        return VariantTemplateMappingItem(
            variant_id=raw.get('variant_id'),
            templates=templates
        )
