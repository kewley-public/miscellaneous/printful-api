from typing import NamedTuple


class SyncProductsPagingResponse(NamedTuple):
    offset: int = 0
    limit: int = 20
    total: int = 0

    @staticmethod
    def from_json(raw: dict):
        return SyncProductsPagingResponse(
            offset=raw.get('offset', 0),
            limit=raw.get('limit', 0),
            total=raw.get('total', 0)
        )
