from typing import NamedTuple


class TaxRateItem(NamedTuple):
    required: bool
    rate: float
    """ In some states shipping price is also included in tax calculation """
    shipping_taxable: bool

    @staticmethod
    def from_json(raw: dict):
        return TaxRateItem(
            required=raw.get('required'),
            rate=raw.get('rate'),
            shipping_taxable=raw.get('shipping_taxable')
        )
