from typing import NamedTuple, List

from printful_api.model.sync.responses.sync_product_response import SyncProductResponse
from printful_api.model.sync.responses.sync_variant_response import SyncVariantResponse


class SyncProductRequestResponse(NamedTuple):
    sync_product: SyncProductResponse
    sync_variants: List[SyncVariantResponse] = []

    @staticmethod
    def from_json(raw: dict):
        return SyncProductRequestResponse(
            sync_product=SyncProductResponse.from_json(raw.get('sync_product', {})),
            sync_variants=[SyncVariantResponse.from_json(x) for x in raw.get('sync_variants', [])]
        )
