from typing import NamedTuple


class OrderCostsItem(NamedTuple):
    """ Total cost of all items """
    subtotal: float
    """ Discount sum """
    discount: float
    """ Shipping costs """
    shipping: float
    """ Sum of taxes (not included in the item price) """
    tax: float
    """ Sum of taxes (not included in the item price) """
    total: float
    """ 3 letter currency code """
    currency: str
    """ Digitization costs """
    digitization: float
    """ Sum of vat (not included in the item price) """
    vat: float

    @staticmethod
    def from_json(raw: dict):
        return OrderCostsItem(
            subtotal=raw.get('subtotal'),
            discount=raw.get('discount'),
            shipping=raw.get('shipping'),
            tax=raw.get('tax'),
            total=raw.get('total'),
            currency=raw.get('currency'),
            digitization=raw.get('digitization'),
            vat=raw.get('vat')
        )

    def to_json(self) -> dict:
        return {
            'discount': self.discount,
            'shipping': self.shipping,
            'tax': self.tax,
            'subtotal': self.subtotal,
            'total': self.total,
            'currency': self.currency,
            'digitization': self.digitization,
            'vat': self.vat
        }
