from typing import NamedTuple, List
from printful_api.model.address_item import AddressItem
from printful_api.model.order.gift_item import GiftItem
from printful_api.model.order.order_costs_item import OrderCostsItem
from printful_api.model.order.order_line_item import OrderLineItem
from printful_api.model.order.packing_slip_item import PackingSlipItem
from printful_api.model.shipment.shipment import Shipment

""" Order is not submitted for fulfillment """
STATUS_DRAFT = 'draft'

""" 
Order was submitted for fulfillment but was not accepted because of an error 
(problem with address, printfiles, charging, etc.) 
"""
STATUS_FAILED = 'failed'

""" Order has been submitted for fulfillment """
STATUS_PENDING = 'pending'

""" Order is canceled """
STATUS_CANCELED = 'canceled'

"""
Order has encountered a problem during the fulfillment that needs to be resolved
together with the Printful customer service
 """
STATUS_ON_HOLD = 'onhold'

""" Order is being fulfilled and is no longer cancellable """
STATUS_IN_PROCESS = 'inprocess'

""" Order is partially fulfilled (some items are shipped already, the rest will follow) """
STATUS_PARTIAL = 'partial'

""" All items are shipped """
STATUS_FULFILLED = 'fulfilled'


class Order(NamedTuple):
    id: int
    external_id: str
    status: str
    """ Shipping method """
    shipping: str
    """ Timestamp """
    created: int
    """ Timestamp """
    updated: int
    recipient: AddressItem
    items: List[OrderLineItem]
    costs: OrderCostsItem
    retail_cost: OrderCostsItem
    shipments: List[Shipment]
    gift: GiftItem
    packing_slip: PackingSlipItem
    """ 
    3 letter currency code (optional), required if the retail costs
    should be converted from another currency instead of USD 
    """
    currency: str = 'USD'

    @staticmethod
    def from_json(raw: dict):
        gift = None
        if raw.get('gift'):
            gift = GiftItem.from_json(raw.get('gift'))

        packing_slip = None
        if raw.get('packing_slip'):
            packing_slip = PackingSlipItem.from_json(raw.get('packing_slip'))

        return Order(
            id=raw.get('id'),
            external_id=raw.get('external_id'),
            status=raw.get('status'),
            shipping=raw.get('shipping'),
            created=raw.get('created'),
            updated=raw.get('updated'),
            recipient=AddressItem.from_json(raw.get('recipient', {})),
            costs=OrderCostsItem.from_json(raw.get('costs', {})),
            retail_cost=OrderCostsItem.from_json(raw.get('retail_costs', {})),
            gift=gift,
            packing_slip=packing_slip,
            items=[OrderLineItem.from_json(x) for x in raw.get('items', [])],
            shipments=[Shipment.from_json(x) for x in raw.get('shipments', [])]
        )

    def to_json(self) -> dict:
        items_json = []
        if len(self.items):
            items_json = [x.to_json() for x in self.items]

        recipient_json = None
        if self.recipient:
            recipient_json = self.recipient.to_json()

        retail_price_json = None
        if self.retail_cost:
            retail_price_json = self.retail_cost.to_json()

        gift_json = None
        if self.gift:
            gift_json = self.gift.to_json()

        packing_slip_json = None
        if self.packing_slip:
            packing_slip_json = self.packing_slip.to_json()

        return {
            'id': self.id,
            'externalId': self.external_id,
            'shipping': self.shipping,
            'status': self.status,
            'created': self.created,
            'updated': self.updated,
            'recipient': recipient_json,
            'items': items_json,
            'retailPrice': retail_price_json,
            'gift': gift_json,
            'packingSlip': packing_slip_json,
            'currency': self.currency
        }
