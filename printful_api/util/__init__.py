def snake_to_camel(word: str):
    first, *rest = word.split('_')
    return first + ''.join(word.capitalize() for word in rest)
