from typing import List

from printful_api import PrintfulApiClient
from printful_api.model.country_item import CountryItem
from printful_api.model.tax_rate_item import TaxRateItem


class TaxRateApi:
    """ API Docs: https://www.printful.com/docs/tax """

    def __init__(self, client: PrintfulApiClient):
        self.client = client

    def get_tax_rate(
            self,
            country_code: str,
            state_code: str,
            city: str,
            zip: str
    ) -> TaxRateItem:
        f"""
        Get tax rate for given address
        
        :param country_code: ISO country code 
        :param state_code: 
        :param city: 
        :param zip: 
        :return: {TaxRateItem}
        """
        recipient = {
            'country_code': country_code,
            'state_code': state_code,
            'city': city,
            'zip': zip
        }
        data = {'recipient': recipient}
        response = self.client.post('tax/rates', data)
        return TaxRateItem.from_json(response)

    def get_tax_countries(self) -> List[CountryItem]:
        f"""
        Retrieve country list (with states) that requires sales tax calculation
        :return: {List[CountryItem]}
        """
        response = self.client.get('tax/countries')
        result_list = []
        for raw in response:
            result_list.append(CountryItem.from_json(raw))
        return result_list
