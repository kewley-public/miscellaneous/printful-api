from typing import NamedTuple, List

from printful_api.model.order.order_item_creation_parameters import OrderItemCreationParameters
from printful_api.model.order.recipient_creation_parameters import RecipientCreationParameters


class OrderCreationParameters(NamedTuple):
    external_id: str
    """ Shipping method. Defaults to 'STANDARD' """
    shipping: str
    recipient: RecipientCreationParameters
    items: List[OrderItemCreationParameters]
    retail_costs: List[any]
    gift: List[any]
    packing_slip: List[any]
    currency: str = 'USD'

    def to_json(self) -> dict:
        return {
            'externalId': self.external_id,
            'shipping': self.shipping
        }

    def to_request_parameters(self) -> dict:
        items = [x.to_json() for x in self.items]
        return {
            'external_id': self.external_id,
            'shipping': self.shipping,
            'recipient': self.recipient.to_json(),
            'retail_costs': self.retail_costs,
            'gift': self.gift,
            'packing_slip': self.packing_slip,
            'currency': self.currency,
            'items': items
        }
