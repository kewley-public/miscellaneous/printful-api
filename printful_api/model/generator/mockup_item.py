from typing import NamedTuple, List, Optional

from printful_api.model.generator.mockup_extra_item import MockupExtraItem


class MockupItem(NamedTuple):
    """ List of Printful product variant ids that this mockup can be used for """
    variant_ids: List[int]
    """ Generated placement. Placements are defined in {Placements} """
    placement: str
    """ Temporary url where generated mockup resides """
    mockup_url: str
    """ Optional list of extra mockups generated. Not all products have extra mockups though. """
    extra_mockups: Optional[List[MockupExtraItem]] = []

    @staticmethod
    def from_json(raw: dict):
        extra_mockups = []
        if raw.get('extra'):
            extra_mockups = [MockupExtraItem.from_json(x) for x in raw.get('extra', [])]

        return MockupItem(
            variant_ids=raw.get('variant_ids', []),
            placement=raw.get('placement'),
            mockup_url=raw.get('mockup_url'),
            extra_mockups=extra_mockups
        )
