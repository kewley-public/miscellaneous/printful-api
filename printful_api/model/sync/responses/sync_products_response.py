from typing import NamedTuple, List

from printful_api.model.sync.responses.sync_product_response import SyncProductResponse
from printful_api.model.sync.responses.sync_products_paging_response import SyncProductsPagingResponse


class SyncProductsResponse(NamedTuple):
    paging: SyncProductsPagingResponse
    result: List[SyncProductResponse]

    @staticmethod
    def from_json(raw: dict):
        return SyncProductsResponse(
            paging=SyncProductsPagingResponse.from_json(raw.get('paging', {})),
            result=[SyncProductResponse.from_json(x) for x in raw.get('result', [])]
        )
