from typing import NamedTuple, List

from printful_api.model.shipment.shipment_Item import ShipmentItem


class Shipment(NamedTuple):
    id: int
    carrier: str
    service: str
    tracking_number: str
    tracking_url: str
    created: str
    ship_date: str
    reshipment: bool
    items: List[ShipmentItem]

    @staticmethod
    def from_json(raw: dict):
        return Shipment(
            id=raw.get('id'),
            carrier=raw.get('carrier'),
            service=raw.get('service'),
            tracking_number=raw.get('tracking_number'),
            tracking_url=raw.get('tracking_url'),
            created=raw.get('created'),
            ship_date=raw.get('ship_date'),
            reshipment=raw.get('reshipment'),
            items=[ShipmentItem.from_json(x) for x in raw.get('items', [])]
        )

    def to_json(self) -> dict:
        items_json = []
        if len(self.items):
            items_json = [x.to_json() for x in self.items]

        return {
            'id': self.id,
            'carrier': self.carrier,
            'service': self.service,
            'trackingNumber': self.tracking_number,
            'trackingUrl': self.tracking_url,
            'created': self.created,
            'shipDate': self.ship_date,
            'reshipment': self.reshipment,
            'items': items_json
        }
